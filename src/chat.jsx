import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import io from 'socket.io-client';
import styled from 'styled-components';


export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      socket: props.socket,
      gameCode: props.gameCode,
      name: props.name,
      playerValue: props.playerValue
    };
  }

  componentWillReceiveProps () {
    this.state.socket.on('message', message => {
      this.setState({ messages: [message, ...this.state.messages]});
    })
  }

  handleSubmit = event => {
    const body = event.target.value;
    if (event.keyCode === 13 && body) {
      const message = {
        gameCode: this.state.gameCode,
        body,
        from: this.state.name || 'Me',
        playerValue: this.state.playerValue
      };
      this.setState({ messages: [message, ...this.state.messages]});
      this.state.socket.emit('message', message);
      event.target.value = '';
    }
  }

  render () {
    const messages = this.state.messages.map((message, index) => {
      return <li key={index}>{message.playerValue}: <B>{message.from} </B>: {message.body}</li>
    })
    return (
      <ContainerChat>
        <Messages>
        {messages}
        </Messages>
        <Input type='text' onKeyUp={this.handleSubmi}/>
        </ContainerChat>
    )
  }
}

const ContainerChat = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: space-around;
  flex-basis: 30%;

`
const Input = styled.input`
  height: 2rem;
  border: none;
  background-color: #e8eaf6;
  color: #1a237e;
  border: 0.1em solid #1a237e;
  border-radius: 3px;
  box-shadow: 2px 2px 4px -2px #1a237e;
`;
const Messages = styled.div`
  display: flex;
  flex-direction: column-reverse;
  border: 0.1em solid #1a237e;
  border-radius: 3px;
  box-shadow: 2px 2px 4px -2px #1a237e;
  list-style: none;
  height: 15rem;
  overflow-y: auto;
  margin-bottom: 0.5rem;
`
const Li = styled.li`
display: inline-flex;
color: #3f51b5;
padding-left: 0.2rem;
margin: 0.1rem 0.2rem 0.1rem;
font-size: 70%;
`
