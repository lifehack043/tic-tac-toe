const webpack = require('webpack');
module.exports = {
  entry: __dirname + '/src/index.js',
  output: {
    path: __dirname + '/',
    filename: 'bundle.js'
  },
  devtool: 'source-maps',
  devServer: {
    hot: true
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['react', 'stage-0'],
          plugins: ['react-html-attrs', 'transform-class-properties', "react-hot-loader/babel",
          ]
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader','css-loader', {
        loader: 'postcss-loader',
        options: {
        plugins: () => [require('autoprefixer')]
        }}]
      },
      {
        test: /\.scss/,
        use: ['style-loader','css-loader', {
        loader: 'postcss-loader',
        options: {
        plugins: () => [require('autoprefixer')]
        }}, 'sass-loader']
      }
    ]
  }
}
