import React, {Component} from 'react'
import io from 'socket.io-client'
import styled, {keyframes} from 'styled-components'
import Game from './components/Game'
import Chat from './components/chat'
import { hot } from 'react-hot-loader'


 class App extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            nameSubmitted: false,
            playerName: '',
            playerNum: '',
            createCode: '',
            joinCode: '',
            start: false,
            created: false,
            gameCode: '',
            winner: '',
            gameEnd: false,
            messages: [],
            winCount: {
              player1: 0,
              player2: 0
            },
            gestures: [
          {
            name: 'lizard',
            img: '../images/lizard.svg',
            win: ['spock', 'paper'],
            playerChoice: false,
            visible: true
          },
          {
            name: 'paper',
            img: '../images/paper.svg',
            win: ['rock', 'spock'],
            playerChoice: false,
            visible: true
          },
          {
            name: 'rock',
            img: '../images/rock.svg',
            win: ['lizard', 'scissors'],
            playerChoice: false,
            visible: true
          },
          {
            name: 'scissors',
            img: '../images/scissors.svg',
            win: ['paper', 'lizard'],
            playerChoice: false,
            visible: true
          },
          {
            name: 'spock',
            img: '../images/spock.svg',
            win: ['rock', 'scissors'],
            playerChoice: false,
            visible: true
          }
        ]
        }
    }
    componentWillMount(){
        this.socket = io('/');
    }
    
    componentDidMount(){
    this.socket.on('game start', (data) => {
      console.log('game start');
      console.log(data);
      this.setState({ gameCode: this.state.createCode,
                      start: true,
                      playerNames: data
      });
      console.log(this.state.playerNames)
      
    });
    this.socket.on('game end', result => {
      this.setState({
        winner: {...this.state.winner, ...result.winner},
        winCount: {...this.state.winCount, ...result.winCount}
      })
      
    })
    this.socket.on('message', message => {
      this.setState({ messages: [message, ...this.state.messages]});
    })
    }
    
    handleSubmitName= event => {
        event.preventDefault();
        this.setState({ nameSubmitted: true});
    }
    handleChangeName= event => {
        console.log(event.target.value)
        this.setState({ playerName: event.target.value })
    }
    
    handleChangeJoin = event => {
    this.setState({ joinCode: event.target.value });
    }
    
    handleSubmitGame = event => {
    event.preventDefault();
    }
  
    createRoom = () => {
        let roomCode = parseInt(Math.random() * (99999 - 11111) + 1);
        this.setState({
            playerNum: 1,
            createCode: roomCode,
            player: 1,
            created: true
        });
        this.socket.emit('create room', {
                                          player1Name: this.state.playerName,
                                          roomCode: roomCode
                                        }
        );
    }
    joinRoom = () => {

        this.socket.emit('join room', {
                                          player2Name: this.state.playerName,
                                          roomCode: this.state.joinCode
                                      }
        );
        this.setState({
            playerNum: 2,
            player: 2,
            createCode: this.state.joinCode,
        });
    }
    
    onPlayerChoice = choiceGesture =>{
      this.setState(prevState => ({
        gestures: prevState.gestures.map(gesture =>
          (gesture.name !== choiceGesture.name) ?
          {
            ...gesture,
            visible: false
          }:
          {
            ...gesture,
            visible: false,
            playerChoice: true
          })
      }))
      
      this.socket.emit('choice gesture', {
                                          gameCode: this.state.gameCode,
                                          playerNum: this.state.playerNum,
                                          playerChoice: choiceGesture
                                          })
    }
    onNewMessage = (message)=>{
      this.setState({ messages: [...this.state.messages, 
                                  {body: message,
                                  from: this.state.playerName
                                }]
        
      });
      this.socket.emit('message', {
                                          gameCode: this.state.gameCode,
                                          body: message,
                                          from: this.state.playerName
                                        })
    }
    
    render(){
        let showNameInput, showCreateRoom, showRoom, showGameAndChat
        
        if (this.state.createCode !== '' && !this.state.start) {
            showRoom =
                <div>
                    <span>Send this code: </span>
                    <h2>{this.state.createCode}</h2>
                    <p>Waiting for opponent...</p>
                </div>
        }
        
        if (!this.state.nameSubmitted){
            showNameInput =
            <Form onSubmit={this.handleSubmitName}>
                <Label>Name:</Label>
                <Input
                    type="text"
                    placeholder={'Your name'}
                    onChange={this.handleChangeName}
                    value={this.state.playerName} required/>
                <Button type="submit">Submit</Button>
            </Form>
            
        }
        
        if (!this.state.start && this.state.nameSubmitted && !this.state.created) {
            showCreateRoom =
            <div>
                <Form onSubmit={this.handleSubmitGame}>
                    <Button onClick={this.createRoom}>Create Game Room</Button>
                    <Label></Label>
                    <Input
                        type="text"
                        placeholder="Insert code here..."
                        value={this.state.joinCode}
                        onChange={this.handleChangeJoin} />
                    <Button type={'submit'} onClick={this.joinRoom}>Join Game</Button>
                </Form>
            </div>
        } else if (!this.state.start && this.state.nameSubmitted && this.state.created) {
            showCreateRoom =
                <Form><Label>{showRoom}</Label></Form>
        }
        
        if (this.state.start) {
          showGameAndChat =
          <GameAndChat>
            <Game gestures={this.state.gestures}
                  gameEnd={this.state.gameEnd}
                  playerNames={this.state.playerNames}
                  winner={this.state.winner}
                  winCount={this.state.winCount}
                  onPlayerChoice={this.onPlayerChoice}/>
            <Chat messages={this.state.messages}
                  onNewMessage={this.onNewMessage}/>
          </GameAndChat>
        }
        
        return(
            <div>
                {showNameInput}
                {showCreateRoom}
                {showGameAndChat}
            </div>
        )
    }
}

// Styled-Components

const fadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const Form = styled.form`
  display: flex;
  flex-flow: row wrap;
  border: 0.1em solid #1a237e;
  padding: 2em;
  max-width: 40em;
  margin: 10em auto;
  border-radius: 3px;
  box-shadow: 2px 2px 4px -2px #1a237e;
  animation: ${fadeIn} 1s linear;
`;

const Input = styled.input`
  flex-grow: 2;
  padding: 1em;
  font-size: 1.5em;
  margin-right: 0.5rem;
  margin-bottom: 0.2em;
  border: none;
  background-color: #e8eaf6;
  color: #1a237e;
`;

const Label = styled.label`
  flex-basis: 100%;
  display: block;
  font-size: 2em;
  margin-bottom: 1em;
  margin-top: 1em;
  text-align: center;
  color: #1a237e;
`;

const Button = styled.button`
  padding: 0.5em 1.2em;
  margin-bottom: 0.2em;
  font-size: 1.5em;
  flex-grow: 1;
  border-radius: 2px;
  border: none;
  outline: none;
  cursor: pointer;
  background-color: #e8eaf6;
  box-shadow: 1px 1px 1px -1px #7986cb;
  color: #1a237e;
  &:hover {
    background-color: #3f51b5;
    color: #e8eaf6;
    box-shadow: 2px 2px 2px #1a237e;
  }
  &:active {
    background-color: #283593;
    color: #e8eaf6;
  }
`;

const GameAndChat = styled.div`
display: flex;
flex-direction: row;
justify-content: space-around;
align-items: center;
align-content: space-around;
height: 100%;
`


export default hot(module)(App)