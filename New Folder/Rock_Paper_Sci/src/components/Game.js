import React from 'react'
import GestureItem from './GestureItem'
import CSSTransition from "react-transition-group/CSSTransition";
import TransitionGroup from "react-transition-group/TransitionGroup";
import '../../public/stylesheets/Game.scss'

const Game=({gestures,winCount,playerNames, onPlayerChoice=f=>f})=>
    
    
    <section className='GameContainer'>
        <div className='Statistic'>
            <label>{playerNames.player1} {winCount.player1} : {winCount.player2} {playerNames.player2}</label>
        </div>
        <TransitionGroup className='Board'>
            
            { gestures.map((gesture, index )=>
                
                <GestureItem  key={index}
                    {...gesture}
                    
                    onPlayerChoice={(choice) => onPlayerChoice(gesture)}/>
                
                )
            }
        </TransitionGroup>
    </section>


export default Game