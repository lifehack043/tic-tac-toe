import React from 'react'
import CSSTransition from "react-transition-group/CSSTransition";
import  '../../public/stylesheets/GestureItem.scss'

class GestureItem extends React.Component {
   
    
    render(){
        const {name, img, visible, onPlayerChoice } = this.props
        
        return(
        <CSSTransition in={visible}
                        mountOnEnter 
                        unmountOnExit
                        onEnter={()=> console.log('hello')}
                        classNames='GestureItem' 
                        timeout={{enter: 2000, exit: 2000}}>
         <img className={'GestureItem'}
         
              src={img} 
              key={name} 
              alt={name} 
              onClick={onPlayerChoice}/>
        </CSSTransition>
        )
    }
}


export default GestureItem