import React from 'react';
import styled, {keyframes} from 'styled-components';


 const Chat =({messages, onNewMessage=f=>f})=> {
   
   const submit = event => {
     if (event.keyCode === 13){
       onNewMessage(event.target.value);
       event.target.value = '';
     }
   };
    const messagesList = messages.map((message, index) => {
      return <Li key={index}> {message.from} : {message.body}</Li>;
    });
    return (
      <ContainerChat>
        <MessagesContainer>
        {messagesList}
        </MessagesContainer>
        <Input type='text' onKeyUp={submit}/>
        </ContainerChat>
    );
  
};

const fadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const ContainerChat = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: space-around;
  flex-basis: 30%;

`;
const Input = styled.input`
  height: 2rem;
  border: none;
  background-color: #e8eaf6;
  color: #1a237e;
  border: 0.1em solid #1a237e;
  border-radius: 3px;
  box-shadow: 2px 2px 4px -2px #1a237e;
`;
const MessagesContainer = styled.div`
  display: flex;
  flex-direction: column-reverse;
  border: 0.1em solid #1a237e;
  border-radius: 3px;
  box-shadow: 2px 2px 4px -2px #1a237e;
  list-style: none;
  height: 30rem;
  overflow-y: auto;
  margin-bottom: 0.5rem;
`;
  const Li = styled.li`
  display: inline-flex;
  color: #3f51b5;
  padding-left: 0.2rem;
  margin: 0.1rem 0.2rem 0.1rem;
  font-size: 100%;
  animation: ${fadeIn} 1s linear;
`;

export default Chat;