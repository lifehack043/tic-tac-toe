const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const socketIo = require('socket.io');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackConfig = require('./webpack.config.js');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

let gameBoardStore = {}

app.use(bodyParser.urlencoded({ extended: false }));


const checkWin = (player1, player2, winCount)=>{
  if (player1.choice !== '' && player2.choice !== ''){
      if ((player1.choice.win).includes(player2.choice.name)) {
          return {winner: 'player1',
                  winCount: { player1: winCount.player1 +1}
                  }
          }
        } if((player2.choice.win).includes(player1.choice.name)){
            return {winner: 'player2',
                    winCount: {player2: winCount.player2 +1}
                  }
          } if((player1.choice.name).includes(player2.choice.name)){
            return {winner: 'pat', winCount}
          }else { console.log('something went wrong')}
        }

io.on('connection', socket =>{
    console.log('connected!');
    socket.on('create room', data => {
    if (data.roomCode) {
      console.log('creating room');
      delete gameBoardStore[data.roomCode];
      gameBoardStore[data.roomCode] = {
                                        winCount: {
                                        player1: 0,
                                        player2: 0
                                      }
      };
      gameBoardStore[data.roomCode].player1 = {
                                          name: data.player1Name,
                                          socketId: socket.id,
                                          choice: ''
                                          };
      socket.join(data.roomCode);
      console.log(data.roomCode);
      console.log('all rooms', gameBoardStore);
      console.log('board of new room', gameBoardStore[data.roomCode]);
      console.log('sending room');
    }
  });

  // connecting player to room (using roomCode given bu host player)

  socket.on('join room', data => {
      if (gameBoardStore.hasOwnProperty(data.roomCode)) {
        if (socket.id !== gameBoardStore[data.roomCode]['player1']) {
          console.log('second player joined');
          gameBoardStore[data.roomCode]['player2'] = {
                                                  name: data.player2Name,
                                                  socketId: socket.id,
                                                  choice: ''
                                                };
          socket.join(data.roomCode);
          io.to(data.roomCode).emit('game start',{
                                              player1: gameBoardStore[data.roomCode].player1.name,
                                              player2: gameBoardStore[data.roomCode].player2.name
          });
        }
      } else {
        console.log('There is no such room in room list');
      }
  });
  
  socket.on('choice gesture', data => {
    if (data.gameCode in gameBoardStore) {
        gameBoardStore[data.gameCode][`player${data.playerNum}`].choice = data.playerChoice;
        console.log(`player${data.playerNum} choice ${data.playerChoice.name}`);
        let { player1, player2, winCount } = gameBoardStore[data.gameCode]
        if(player1.choice !== '' && player2.choice !== ''){
          let result = checkWin(player1, player2, winCount)
          gameBoardStore[data.gameCode].winCount = {...gameBoardStore[data.gameCode].winCount, 
                                                    ...result.winCount}
          io.to(data.gameCode).emit('game end', result)
        }
        
    }
  })
  
  socket.on('message', data => {
    socket.broadcast.to(data.gameCode).emit('message', {
      body: data.body,
      from: data.from
    })
  })
})

app.use(express.static(__dirname + '/public'));
app.use(webpackDevMiddleware(webpack(webpackConfig), { stats: { colors: true }}));

server.listen(process.env.PORT || 8080);