import React, {Component} from 'react';
import io from 'socket.io-client';
import Game from './components/Game';
import Chat from './components/chat';
import { CSSTransition } from 'react-transition-group'
import '../public/stylesheets/App.scss';
import '../public/stylesheets/animation.scss';


 class App extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            activateNameForm: false,
            nameSubmitted: false,
            activateWaitMessage: false,
            activateCreateOrJoinForm: false,
            gameCode: 0,
            playerName: '',
            playerNum: '',
            anotherPlayerNum: '',
            createCode: '',
            joinCode: '',
            start: false,
            messages: [],
        };
        
    }
    componentWillMount(){
        this.socket = io('/');
    }
    
    componentDidMount(){
      this.setState({activateNameForm: true});
    
      this.socket.on('game start', (data) => {
        console.log('game start');
        this.setState({ gameCode: this.state.createCode,
                        playerNames: data,
                        activateWaitMessage: false,
                        activateCreateOrJoinForm: false,
                        start: true
        });
      });   
        
      this.socket.on('message', message => {
        this.setState({ messages: [message, ...this.state.messages]});
      });
    }
    
    handleSubmitName= event => {
        event.preventDefault();
        this.setState({ activateNameForm: false, 
                        activateCreateOrJoinForm: true
                     });
        
    }
    handleChangeName= event => {
        event.preventDefault();
        this.setState({ playerName: event.target.value });
    }
    
    handleChangeJoin = event => {
      this.setState({ joinCode: event.target.value });
    }
    
    createRoom = (event) => {
      event.preventDefault();
        let roomCode = parseInt(Math.random() * (99999 - 11111), 10);
        this.setState({
            playerNum: 1,
            anotherPlayerNum: 2,
            createCode: roomCode,
            activateCreateOrJoinForm: false,
            activateWaitMessage: true
        });
        this.socket.emit('create room', {
                                          player1Name: this.state.playerName,
                                          roomCode: roomCode
                                        }
        );
    }
    
    joinRoom = (event) => {
        event.preventDefault();
      this.socket.emit('join room', {
                                      player2Name: this.state.playerName,
                                      roomCode: this.state.joinCode
                                    }
      );
      this.setState({
        playerNum: 2,
        anotherPlayerNum: 1,
        createCode: this.state.joinCode
      });
    }
  
    onNewMessage = (message)=>{
      this.setState({ messages: [...this.state.messages, 
                                  {body: message,
                                  from: this.state.playerName
                                }]
      });
      
      this.socket.emit('message', {gameCode: this.state.gameCode,
                                   body: message,
                                   from: this.state.playerName
                                  });
      }
      
    render(){

        return(
          <div>
          <CSSTransition in={this.state.activateNameForm}
                         mountOnEnter
                         unmountOnExit
                         timeout={1000}
                         timeout={{enter:1000, exit: 0}}
                         classNames='fade'>
            <form className='enterForms' onSubmit={this.handleSubmitName}>
                <label className='enterFormsLabel' >Name:</label>
                <input
                    type="text"
                    className='enterFormsInput' 
                    placeholder={'Your name'}
                    onChange={this.handleChangeName}
                    value={this.state.playerName} required/>
                <button className='enterFormsButton' >Submit</button>
            </form>
          </CSSTransition>  
          
          <CSSTransition in={this.state.activateCreateOrJoinForm}
                         mountOnEnter
                         unmountOnExit
                         timeout={{enter:1000, exit: 0}}
                         classNames='fade'>
              <form className='enterForms' >
                  <button className='enterFormsButton' onClick={this.createRoom}>Create Game Room</button>
                    <label className='enterFormsLabel'></label>
                    <input className='enterFormsInput' type="text"
                        placeholder="Insert code here..."
                        value={this.state.joinCode}
                        onChange={this.handleChangeJoin} />
                    <button className='enterFormsButton' type='submit' onClick={this.joinRoom}>Join Game</button>
                </form>
          </CSSTransition>
          
          <CSSTransition in={this.state.activateWaitMessage}
                         mountOnEnter
                         unmountOnExit
                         timeout={1000}
                         classNames='fade'>
                <form className='enterForms' >
                    <label className='enterFormsLabel'>
                        <span>Send this code: </span>
                        <h2>{this.state.createCode}</h2>
                        <p>Waiting for opponent...</p>
                    </label>
                </form>
          </CSSTransition>
          
          <CSSTransition in={this.state.start}
                         className='GameAndChat'
                         mountOnEnter
                         unmountOnExit
                         timeout={1000}
                         classNames='fade'>
            <div className='GameAndChat'>            
                <Game playerNum={this.state.playerNum}
                      anotherPlayerNum={this.state.anotherPlayerNum}
                      playerName={this.state.playerName}
                      socket={this.socket}
                      gameCode={this.state.gameCode}
                      playerNames={this.state.playerNames}/>
                  
                <Chat messages={this.state.messages}
                    onNewMessage={this.onNewMessage}/>
            </div>
          </CSSTransition>;
          </div>
        );
    }
}

export default App;