import React from 'react'
import { CSSTransition } from 'react-transition-group'
import initialStateGestures from '../data/initialStateGestures'
import '../../public/stylesheets/Game.scss';

class Game extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            playerName: props.playerName,
            playerNum: props.playerNum,
            anotherPlayerNum: props.anotherPlayerNum,
            playerNames: props.playerNames,
            socket: props.socket,
            gameCode: props.gameCode, 
            winCount: {
              player1: 0,
              player2: 0 },
            playersChoice: {
              player1: '',
              player2: ''},
            readyButton: false,
            showGestures: false,
            showChoicedGestures: false,
            sendChoice: false,
            waitAnotherPlayer: false,
            showWaitAnotherPlayer: false,
            winner: '',
            gestures: initialStateGestures,
        };
        this.onShowChoicedGestures = this.onShowChoicedGestures.bind(this);
        this.onWaitAnotherPlayer = this.onWaitAnotherPlayer.bind(this);
        this.onShowGestures = this.onShowGestures.bind(this);
    }
    
    componentDidMount(){
        this.setState({
            showGestures: true
        });
        
        
        this.state.socket.on('game end', result => {
        this.setState({
          playersChoice: {...this.state.playersChoice,...result.playersChoice},
          waitAnotherPlayer: false,
          showWaitAnotherPlayer: false,
          sendChoice: false,
          winner: result.winner,
          winCount: {...this.state.winCount, ...result.winCount}
          });
          console.log(`player${this.stateplayerNum} ${this.state.waitAnotherPlayer} ${this.state.showWaitAnotherPlayer}`)
        });
        
        this.state.socket.on('continue game', data =>{
            this.setState(prevState => ({
                showChoicedGestures: false,
                readyButton: false,
                gestures: initialStateGestures,
            }));
        });
        
        this.state.socket.on('waitAnotherPlayer', data => {
            console.log(data);
            this.setState({waitAnotherPlayer: true});
        });
    }
   
    onPlayerChoice = choiceGesture =>{
        console.log(choiceGesture);
        this.setState({
              sendChoice: true,
              showGestures: false,
              playersChoice:{player1: '', player2: ''},
              showWaitAnotherPlayer: true
         });
        this.state.socket.emit('choice gesture', {
                                          gameCode: this.state.gameCode,
                                          playerNum: this.state.playerNum,
                                          anotherPlayerNum: this.state.anotherPlayerNum,
                                          playerChoice: choiceGesture
        });
    }
        
    onContinueGame=()=>{
        this.setState({readyButton: !this.state.readyButton});
        this.state.socket.emit('continue game',{
          gameCode: this.state.gameCode,
          playerNum: this.state.playerNum
          });
        }
        
    onWaitAnotherPlayer=()=>{
       this.setState({waitAnotherPlayer: true});

    }
    
    onShowChoicedGestures=()=>{
        this.setState({showChoicedGestures: true});
    }
    
    onShowGestures=()=>{
        this.setState({showGestures: true});
    }
    
    render(){
        
        return(
            
    <section className='GameContainer'>
    
        <div className='Statistic'>
            <label>
                {this.state.playerNames.player1} {this.state.winCount.player1} : 
                {this.state.winCount.player2} {this.state.playerNames.player2}
            </label>
        </div>
        
        
        <div className='Board'>
            
            <CSSTransition in={this.state.waitAnotherPlayer && this.state.showWaitAnotherPlayer}
                            key='waitAnotherPlayer1'
                            mountOnEnter
                            unmountOnExit
                            timeout={{enter:1000, exit: 0}}
                            onExited={this.onShowChoicedGestures}
                            classNames='fade'>
                <span className='information'>
                    Wait {this.state.playerNames[`player${this.state.anotherPlayerNum}`]} turn...
                </span>
            
            </CSSTransition>
              
            <CSSTransition in={this.state.showChoicedGestures}
                            key='showWinner'
                            mountOnEnter
                            unmountOnExit
                            timeout={1000}
                            onExited={this.onShowGestures}
                            classNames='fade'>
                <div className='choiceContainer'>
            
                    <span className='information'> {`${this.state.winner}`} win this game! </span>            
                
                    <div className='choicedGestures'>
                
                        <div className='choicedGesture'>
                            <img className='imageChoicedGesture' 
                                src={this.state.playersChoice[`player${this.state.playerNum}`]}/> 
                            <label className='labelChoicedGesture'>Your choice</label>
                        </div>
                    
                        <div className='choicedGesture'>
                            <img className='imageChoicedGesture' 
                                src={this.state.playersChoice[`player${this.state.anotherPlayerNum}`]}/>
                            <label className='labelChoicedGesture'>Opponent choice</label>
                        </div>
                    
                    </div>
                    
                    <div className='wrapperReadyButton'>              
                        <button onClick={this.onContinueGame.bind(this)}
                            className={this.state.readyButton ? 'unreadyButton':'readyButton'}>Ready</button>
                    </div>    
                </div>
            </CSSTransition>
            
            <CSSTransition  in={this.state.showGestures}
                            mountOnEnter
                            unmountOnExit
                            timeout={{enter:1000, exit: 0}}
                            onExited={(!this.state.waitAnotherPlayer) ? this.onWaitAnotherPlayer : this.onShowChoicedGestures}
                            classNames='fade'>
                <div className='gesturesList'>
                    {this.state.gestures.map(gesture => 
                        
                                 <img className='gestureImage'
                                 key={gesture.name}
                                 src={gesture.img} 
                                 alt={gesture.name} 
                                 onClick={this.onPlayerChoice.bind(this, gesture)} />
                        
                    )}
                </div>
            </CSSTransition>    
            </div>
        </section>
)}}

export default Game;