import React from 'react';
import '../../public/stylesheets/chat.scss';

 const Chat =({messages, onNewMessage=f=>f})=> {
   
   const submit = event => {
     if (event.keyCode === 13){
       onNewMessage(event.target.value);
       event.target.value = '';
     }
   };
    const messagesList = messages.map((message, index) => {
      
      return <li key={index}> {message.from} : {message.body}</li>;
      
    });
    return (
      <div className='containerChat'>
        <div className='messagesContainer'>
          {messagesList}
        </div>
        <input className='messageInput' type='text' onKeyUp={submit}/>
      </div>
    );
  
};



export default Chat;